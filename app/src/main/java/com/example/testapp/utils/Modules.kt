package com.example.testapp.utils

import com.example.testapp.repository.MainActivityRepository
import com.example.testapp.repository.remote.ApiInterface
import com.example.testapp.views.MainActivityViewModel
import com.example.testapp.views.adapters.AllGameAdapter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.component.KoinApiExtension
import org.koin.dsl.module

@OptIn(KoinApiExtension::class)
val appModule = module {
    single { ResponseHandler() }

    single { ApiInterface.create() }

    single { MainActivityRepository(service = get(), responseHandler = get()) }

    viewModel { MainActivityViewModel(repository = get()) }

    factory { AllGameAdapter() }
}