package com.example.testapp.utils

import com.google.gson.Gson
import kotlinx.coroutines.CancellationException
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException
import kotlin.Exception

// Note delete logE and printstacktrace later
class ResponseHandler {
    inline fun<reified T> handleResponse(call: () -> Response<T>): Result<T> {
        val simpleName = this::class.java.simpleName
        try {
            val response = call.invoke()
            logE("Thread", "I'm working in thread ${Thread.currentThread().name}")
            logE(simpleName, "Response body: ${response.body()}")
            logE(simpleName, "Response code: ${response.code()}")
            try {
                return when {
                    response.isSuccessful -> {
                        logE(simpleName, response.body().toString())
                        Result.Success(response.body()!!)
                    }
                    !response.isSuccessful -> {
                        response.errorBody()?.let {
                            logE(simpleName, "${response.code()} ${response.message()}")
                            val errorResponse = Gson().fromJson(it.charStream(), T::class.java)
                            return Result.Error(errorResponse, "${response.code()} ${response.message()}")
                        } ?: return Result.Error(null, "${response.code()} ${response.message()}")
                    }
                    else -> {
                        Result.Loading()
                    }
                }
            } catch (e: HttpException) {
                logE("$simpleName: HttpException", e.cause.toString())
                logE("$simpleName: HttpException", e.message().toString())
                e.printStackTrace()
                return Result.Error(null, "HttpException: ${e.message()}")
            } catch (e: IOException) {
                logE("$simpleName: IOException", e.cause.toString())
                logE("$simpleName: IOException", e.message.toString())
                e.printStackTrace()
                return Result.Error(null, "IOException: ${e.message}")
            } catch (e: Exception) {
                logE("$simpleName: Exception", e.cause.toString())
                logE("$simpleName: Exception", e.message.toString())
                e.printStackTrace()
                return Result.Error(null, "Something went wrong: ${e.message}")
            }
        } catch (t: Throwable) {
            return when (t) {
                is SocketTimeoutException -> {
                    logE("$simpleName: SocketTimeoutException", t.message.toString())
                    t.printStackTrace()
                    Result.Error(null, "Timeout: Mohon periksa internet anda")
                }
                is CancellationException -> {
                    logE("$simpleName: CancellationException", t.message.toString())
                    t.printStackTrace()
                    Result.Error(null, "Canceled")
                }
                is Exception -> {
                    logE("$simpleName: Exception", "message: ${t.message}")
                    logE("$simpleName: Exception", "cause: ${t.cause}")
                    t.printStackTrace()
                    Result.Error(null, "Something went wrong: ${t.message}")
                }
                else -> {
                    logE("$simpleName: UnknownException", t.message.toString())
                    t.printStackTrace()
                    Result.Error(null, "UnknownException: ${t.message}")
                }
            }
        }
    }
}