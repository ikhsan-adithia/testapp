package com.example.testapp.utils

import android.util.Log

object Extensions {
    const val BASE_URL = "https://alpha.streamgaming.id"
}

fun logE(tag: String, msg: String) {
    Log.e(tag, msg)
}