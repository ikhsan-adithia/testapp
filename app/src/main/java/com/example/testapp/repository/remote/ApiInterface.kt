package com.example.testapp.repository.remote

import com.example.testapp.utils.Extensions.BASE_URL
import com.example.testapp.views.models.AllGameModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit

interface ApiInterface {

    @GET("api/game/static")
    suspend fun getAllGame(
        @Query("search") search: String?
    ): Response<AllGameModel>

    companion object {
        fun create(): ApiInterface {
            val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
                this.level =HttpLoggingInterceptor.Level.BODY
            }

            val builder: OkHttpClient.Builder = OkHttpClient.Builder()
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES)

            val client: OkHttpClient = builder.apply {
                this.addInterceptor(interceptor)
            }.build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }
}