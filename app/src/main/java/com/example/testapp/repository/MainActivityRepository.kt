package com.example.testapp.repository

import com.example.testapp.repository.remote.ApiInterface
import com.example.testapp.utils.ResponseHandler

class MainActivityRepository(
    private val service: ApiInterface,
    private val responseHandler: ResponseHandler
) {
    suspend fun getAllGame(search: String?) =
        responseHandler.handleResponse { service.getAllGame(search) }
}