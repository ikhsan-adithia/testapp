package com.example.testapp.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testapp.R
import com.example.testapp.views.models.GameItem
import kotlinx.android.synthetic.main.game_item.view.*

class AllGameAdapter: RecyclerView.Adapter<AllGameAdapter.AllGameViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<GameItem>() {
        override fun areItemsTheSame(oldItem: GameItem, newItem: GameItem): Boolean {
            return oldItem.idGameList == newItem.idGameList
        }

        override fun areContentsTheSame(oldItem: GameItem, newItem: GameItem): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, diffCallback)

    inner class AllGameViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllGameViewHolder {
        return AllGameViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.game_item, parent, false))
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: AllGameViewHolder, position: Int) {
        val gameItem = differ.currentList[position]

        holder.itemView.apply {
            this.tv_idGame_GameItem.text = gameItem.idGameList.toString()
            this.tv_gameName_GameItem.text = gameItem.name
        }
    }
}