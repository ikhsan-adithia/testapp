package com.example.testapp.views.models

import com.google.gson.annotations.SerializedName

data class AllGameModel(

	@field:SerializedName("data")
	val gameList: List<GameItem?>? = null
)

data class GameItem(

	@field:SerializedName("background_logo1")
	val backgroundLogo1: String? = null,

	@field:SerializedName("background_logo2")
	val backgroundLogo2: String? = null,

	@field:SerializedName("full_url")
	val fullUrl: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("id_game_list")
	val idGameList: Int? = null,

	@field:SerializedName("url")
	val url: String? = null
)
