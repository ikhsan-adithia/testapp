package com.example.testapp.views

import androidx.lifecycle.*
import com.example.testapp.repository.MainActivityRepository
import com.example.testapp.utils.Result
import com.example.testapp.views.models.AllGameModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.IO_PARALLELISM_PROPERTY_NAME
import kotlinx.coroutines.launch
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent

@KoinApiExtension
class MainActivityViewModel(private val repository: MainActivityRepository): ViewModel(), KoinComponent {

    private val _allGameData = MutableLiveData<Result<AllGameModel>>()

    fun getAllGame(search: String? = null) = liveData(Dispatchers.IO) {
        emit(repository.getAllGame(search))
    }

    fun getAllGameV2(search: String? = null) {
        viewModelScope.launch(IO) {
            val allGameData = repository.getAllGame(search)
            _allGameData.postValue(allGameData)
        }
    }

    fun getAllGameV2LiveData(): LiveData<Result<AllGameModel>> = _allGameData
}