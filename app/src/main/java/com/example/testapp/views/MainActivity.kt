package com.example.testapp.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.testapp.R
import com.example.testapp.databinding.ActivityMainBinding
import com.example.testapp.utils.Result
import com.example.testapp.views.adapters.AllGameAdapter
import com.example.testapp.views.models.AllGameModel
import org.koin.android.ext.android.inject
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class MainActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MainActivityViewModel by inject()

    private val gameAdapter: AllGameAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getAllGameObserver?.let {
//            viewModel.getAllGame("Valorant")
            viewModel.getAllGame().observe(this, it)
        }
    }

    private var getAllGameObserver: Observer<Result<AllGameModel>>? = Observer { result ->
        when (result) {
            is Result.Loading -> {}
            is Result.Success -> {
                binding.rvGameListHome.apply {
                    this.adapter = gameAdapter
                    result.data?.gameList?.let {
                        gameAdapter.differ.submitList(it.filterNotNull())
                    }
                }
            }
            is Result.Error -> {}
        }
    }
}